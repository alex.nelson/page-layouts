(() => {
  const css = `
		bg-grid,
		.cardpage-main,
		.page-body,
		.badge-container,
		.bg-grid_badge-container,
		.badge {
		    width: 100% !important;
		    height: 100% !important;
		}

		.cardpage-main,
		.page-body,
		.badge-container {
		    position: absolute !important;
		    top: 0;
		    left: 0;
		}

		.badge-container {
		    margin: 0 !important;
		    padding: 0 !important;
		}

		.badge .badge_content {
		    height: 100%;
		}

		.badge .dm-badge {
		    min-height: 100%;
		    min-width: 100% !important;
		}

		.badge .badge_frame {
		    margin: 0;
		    border-radius: 0 !important;
		    border: none !important;
		    width: 100% !important;
		    height: 100% !important;
		}

		.pendo-wrapper,
		.page-header,
		.badge_frame .badge_header,
		.badge_frame .badge_footer {
		    visibility: hidden;
		}
	`;

  const head = document.head || document.getElementsByTagName('head')[0];
  const style = document.createElement('style');
  style.type = 'text/css';
  style.setAttribute('extension', 'page-layout');
  style.appendChild(document.createTextNode(css));
  head.appendChild(style);
  document.PAGE_LAYOUT = true;
})();
