(() => {
	const style = document.querySelector('style[extension=page-layout');
  const head = document.head || document.getElementsByTagName('head')[0];

	if (style) { 
		head.removeChild(style);
		document.PAGE_LAYOUT = false;
	}
})();