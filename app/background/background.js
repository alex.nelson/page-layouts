chrome.browserAction.onClicked.addListener(tab => {
	chrome.tabs.executeScript(tab.id, { 
		code: 'document.PAGE_LAYOUT === true;',
		runAt: 'document_end' 
	}, ([isInPageLayout]) => {
		const script = isInPageLayout
			? 'content/removeCss.js'
			: 'content/insertCss.js';

		chrome.tabs.executeScript(tab.id, {
			file: script,
			runAt: 'document_end'
		});
	});

	// chrome.tabs.insertCSS(tab.id, { 
	// 	file: 'content/app.css',
	// 	runAt: 'document_end',
	// });
});